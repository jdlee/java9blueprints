module dupefind.lib {
    exports com.steeplesoft.j9bp.dupefind.lib;
    exports com.steeplesoft.j9bp.dupefind.lib.model;

    requires java.logging;
    requires javax.persistence;
}
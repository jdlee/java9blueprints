/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.photobeans.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author jason
 */
public class PathTreeItem {

    private String name;
    private PathTreeItem parent;
    private final List<PathTreeItem> children = new ArrayList<>();

    public PathTreeItem(PathTreeItem parent, String name) {
        this.name = name;
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public List<PathTreeItem> getChildren() {
        return children;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.parent);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PathTreeItem other = (PathTreeItem) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.parent, other.parent)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PathTreeItem{" + "name=" + name + ", parent=" + (parent != null ? parent.getName() : "null")
                + ", children=" + children + '}';
    }
}

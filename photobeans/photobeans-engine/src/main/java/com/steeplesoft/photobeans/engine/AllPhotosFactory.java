/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.photobeans.engine;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

/**
 *
 * @author jason
 */
public class AllPhotosFactory extends ChildFactory<PathTreeItem> {

    private PathTreeItem item;

    public AllPhotosFactory() {
    }

    public AllPhotosFactory(PathTreeItem item) {
        this.item = item;
    }

    @Override
    protected boolean createKeys(List<PathTreeItem> toPopulate) {
        if (item == null) {
            List<String> paths = new ArrayList<String>() {
                {
                    add("P:\\one\\two\\three\\four\\image1.png");
                    add("P:\\one\\two\\three\\five\\image2.png");
                    add("P:\\one\\six\\seven\\image3.png");
                    add("P:\\one\\two\\three\\four\\image4.png");
                    add("P:\\eight\\nine\\ten\\eleven\\twelve\\image5.png");
                }
            };
            toPopulate.addAll(createPathTreeItems(paths));
        } else {
            toPopulate.addAll(item.getChildren());
        }

        return true;
    }

    private List<PathTreeItem> createPathTreeItems(List<String> paths) {
        List<PathTreeItem> files = new ArrayList<>();

        paths.forEach((path) -> {
            PathTreeItem parent = null;
            File f = new File(path);
            Path p = FileSystems.getDefault().getPath(path);
            Iterator<Path> i = p.iterator();
            while (i.hasNext()) {
//                System.out.println(i.next().toString());
//            }
//            for (String part : f.list()) { //path.split(File.separator)) {
                String part = i.next().toString();
                if (part.isEmpty()) {
                    continue;
                }
                PathTreeItem filePart = new PathTreeItem(parent, part);
                if (files.contains(filePart)) {
                    filePart = files.get(files.indexOf(filePart));
                } else if (parent != null && parent.getChildren().contains(filePart)) {
                    filePart = parent.getChildren().get(parent.getChildren().indexOf(filePart));
                } else {
                    if (parent == null) {
                        files.add(filePart);
                    } else {
                        parent.getChildren().add(filePart);
                    }
                }
                parent = filePart;
            }
        });
        return files;
    }

    @Override
    protected Node createNodeForKey(PathTreeItem key) {
        return new PathTreeItemNode(key);
    }

}

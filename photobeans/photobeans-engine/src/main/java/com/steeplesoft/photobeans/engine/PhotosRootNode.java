/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.photobeans.engine;

import java.util.logging.Level;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.NbBundle;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

/**
 *
 * @author jason
 */
@NbBundle.Messages({
        "HINT_PhotosRootNode=Shows all photos",
        "LBL_PhotosRootNode=Photos"
    })
class PhotosRootNode extends AbstractNode {
    public PhotosRootNode() {
        this(new InstanceContent());
    }
        
    private PhotosRootNode(InstanceContent ic) {
        super(Children.create(new AllPhotosFactory(), true),
                new AbstractLookup(ic));
        this.setIconBaseWithExtension("com/asgteach/familytree/utilities/personIcon.png");
        this.setDisplayName(Bundle.LBL_PhotosRootNode());
        this.setShortDescription(Bundle.HINT_PhotosRootNode());
//        this.instanceContent = ic;
        
//        this.capabilities = new PersonCapability();
//        this.personType = new PersonType(capabilities, this, true);
//        this.instanceContent.add(personType);

        // Add a new ability for this node to be reloaded
        /*
        this.instanceContent.add((ReloadableViewCapability) () -> {
            logger.log(Level.FINER, "ReloadableViewCapability: reloadChildren");
            // To reload this node, just set a new set of children
            // using RootNodeChildFactory object, that retrieves
            // children asynchronously
            setChildren(Children.create(new RootNodeChildFactory(),
                    true));
        });
        */
        
    }
}

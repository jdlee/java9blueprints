/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.photobeans.engine;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author jason
 */
public class PathTreeItemNode extends AbstractNode implements PropertyChangeListener {
    
    private final InstanceContent instanceContent;
    private final PathTreeItem item;

    public PathTreeItemNode(PathTreeItem item) {
        this(item, new InstanceContent());
    }

    public PathTreeItemNode(PathTreeItem item, InstanceContent ic) {
        super(isFileNode(item) ? 
                    Children.LEAF : 
                    Children.create(new AllPhotosFactory(item), true), 
                Lookups.singleton(item));
        this.item = item;
        instanceContent = ic;
        instanceContent.add(item);
        setDisplayName(item.getName());
        instanceContent.add(new PhotosCapability() {
            @Override
            public void doPhoto() {
                System.out.println("hi");
            }
        });
    }

    @SuppressWarnings(value = "unchecked")
    @Override
    public Action[] getActions(boolean context) {
        if (!isFileNode(item)) {
            return super.getActions(true);
        } else {
            return new Action[]{new MyAction()};
        }
        //            List<Action> photoActions = new ArrayList<>(Arrays.asList(super.getActions(context)));
        //            photoActions.addAll(Utilities.actionsForPath("Actions/Nodes"));
        //            return photoActions.toArray(new Action[photoActions.size()]);
    }

    private static boolean isFileNode(PathTreeItem item) {
        return item.getName().contains(".");
    }

    @SuppressWarnings(value = "unchecked")
    @Override
    public Action getPreferredAction() {
        return new MyAction();
        //            List<Action> photoActions = (List<Action>) Utilities.actionsForPath("Actions/Nodes");
        //            if (!photoActions.isEmpty()) {
        //                return photoActions.get(0);
        //            } else {
        //                return super.getPreferredAction();
        //            }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        //            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private class MyAction extends AbstractAction {

        public MyAction() {
            putValue(NAME, "Do Something");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            PathTreeItem obj = getLookup().lookup(PathTreeItem.class);
            JOptionPane.showMessageDialog(null, "Hello from " + obj);
        }
    }
    
}

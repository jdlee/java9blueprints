package com.steeplesoft.j9bp.photomgmt;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.util.Callback;

public class FXMLController implements Initializable {

    @FXML
    private TreeView<PathTreeItem> treeView;

    @FXML
    private void handleButtonAction(ActionEvent event) {
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        DragResizerXY.makeResizable(treeView);
        List<String> paths = new ArrayList<String>() {
            {
                add("/one/two/three/four/image1.png");
                add("/one/two/three/five/image2.png");
                add("/one/six/seven/image3.png");
                add("/one/two/three/four/image4.png");
                add("/eight/nine/ten/eleven/twelve/image5.png");
            }
        };
        treeView.setRoot(new TreeItem<>(new PathTreeItem(null, "root")));
        treeView.setShowRoot(false);
        treeView.getRoot().setExpanded(true);
        treeView.setCellFactory(p -> new PathTreeItemCell());

        createPathTreeItems(paths).forEach((root) -> {
            findFiles(root, null);
        });
    }

    private  List<PathTreeItem> createPathTreeItems(List<String> paths) {
        List<PathTreeItem> files = new ArrayList<>();

        paths.forEach((path) -> {
            PathTreeItem parent = null;
            for (String part : path.split("/")) {
                if (part.isEmpty()) {
                    continue;
                }
                PathTreeItem filePart = new PathTreeItem(parent, part);
                if (files.contains(filePart)) {
                    filePart = files.get(files.indexOf(filePart));
                } else if (parent != null && parent.getChildren().contains(filePart)) {
                    filePart = parent.getChildren().get(parent.getChildren().indexOf(filePart));
                } else {
                    if (parent == null) {
                        files.add(filePart);
                    } else {
                        parent.getChildren().add(filePart);
                    }
                }
                parent = filePart;
            }
        });
        return files;
    }

    private List<File> createFilesFromPaths(List<String> paths) {
        List<File> files = new ArrayList<>();
        paths.forEach((path) -> {
            File parent = null;
            for (String part : path.split("/")) {
                if (part.isEmpty()) {
                    continue;
                }
                File filePart = new File(parent, part);
                if (files.contains(filePart)) {
                    filePart = files.get(files.indexOf(filePart));
                } else {
                    if (parent == null) {
                        files.add(filePart);
                    }
                }
                parent = filePart;
            }
        });
        return files;
    }

    private void findFiles(PathTreeItem item, TreeItem<PathTreeItem> parent) {
        TreeItem<PathTreeItem> root = new TreeItem<>(item);
        root.setExpanded(true);
        for (PathTreeItem child : item.getChildren()) {
            if (!child.getChildren().isEmpty()) {
                findFiles(child, root);
            } else {
                root.getChildren().add(new TreeItem<>(child));
            }

        }
        if (parent == null) {
            treeView.getRoot().getChildren().add(root);
        } else {
            parent.getChildren().add(root);
        }
    }

}

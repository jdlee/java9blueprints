/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.j9bp.photomgmt;

import javafx.scene.control.TreeCell;

/**
 *
 * @author jason
 */
class PathTreeItemCell extends TreeCell<PathTreeItem> {
    
    public PathTreeItemCell() {
    }

    @Override
    protected void updateItem(PathTreeItem item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setText(item.getName());
        } else {
            setText(null);
        }
    }
    
}

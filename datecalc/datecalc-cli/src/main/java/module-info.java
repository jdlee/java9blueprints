module datecalc.cli {
    requires datecalc.lib;
    requires tomitribe.crest;
    requires tomitribe.crest.api;
    
    exports com.steeplesoft.j9bp.datecalc.cli;
}

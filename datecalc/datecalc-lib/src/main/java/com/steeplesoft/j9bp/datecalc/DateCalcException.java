package com.steeplesoft.j9bp.datecalc;

/**
 *
 * @author jason
 */
public class DateCalcException extends RuntimeException {
    public DateCalcException(String message) {
        super(message);
    }
    
}

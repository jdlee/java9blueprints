/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.j9bp.sunago;

import com.steeplesoft.j9bp.sunago.api.SunagoPreferences;
import java.util.Iterator;
import java.util.ServiceLoader;

/**
 *
 * @author jason
 */
public class SunagoUtil {
    private static SunagoPreferences preferences;

    public static synchronized SunagoPreferences getSunagoPreferences() {
        if (preferences == null) {
            ServiceLoader<SunagoPreferences> spLoader = ServiceLoader.load(SunagoPreferences.class);
            Iterator<SunagoPreferences> iterator = spLoader.iterator();
            preferences = iterator.hasNext() ? iterator.next() : null;
        }
        return preferences;
    }
}

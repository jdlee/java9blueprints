/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.j9bp.sunago;

/**
 *
 * @author jason
 */
public enum SunagoPrefsKeys {
    ITEM_COUNT("itemCount");
    private final String key;

    SunagoPrefsKeys(String key) {
        this.key = key;
    }

    public String getKey() {
        return "sunago." + key;
    }

}
